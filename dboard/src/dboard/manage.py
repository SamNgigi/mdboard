#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys


CMDLINE = ' '.join(sys.argv)

if any(word in CMDLINE for word in {'jupyter', 'notebook'}):
    print("📌  Ensuring DJANGO_ALLOW_ASYNC_UNSAFE is set for Jupyter...")
    os.environ.setdefault("DJANGO_ALLOW_ASYNC_UNSAFE", "1")


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                          'dboard.config.settings.dev')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to run poetry run or poetry shell?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
